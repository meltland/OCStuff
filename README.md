# OC Stuff

Scripts I've made for the [OpenComputers Minecraft mod](https://modrinth.com/mod/opencomputers).

## DTF
Transfer text files over networks! It's a little bit finicky, should not be used near or in networks that are active!

## Imgload
Load "TBL" and "CLF" image files. To convert a regular image into a CLF image, see the "imgload converter" Python script. TBL images lack resolution data, you should use CLF images instead.

### CLF format
This is a table that contains the width, height, and data for each pixel in the image. A black 2x3 image would be
```
{2,3,0,0,0,0,0,0}
```

### TBL format
This is a table that contains the data for each pixel in the image. A black image for a 2x3 resolution screen would be
```
{0,0,0,0,0,0}
```

## Bep
Takes a fancy table of beeps and plays them.
There's an example bep file in the beps directory. Or maybe more than one.

## Battery

Battery percentage and raw numbers