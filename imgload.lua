local component = require("component")
local shell = require("shell")
local gpu = component.gpu
local event = require("event")
local filesystem = require("filesystem")

if not filesystem.exists("/lib/tableToFile.lua") then
  print("tableToFile library not found! Attempting to download it using wget...")
  os.execute("wget https://gitlab.com/meltland/OCStuff/-/raw/main/tableToFile.lua /lib/tableToFile.lua")
end

local ttf = require("tableToFile")

-- gpu.setResolution(160, 50)

local help = "IMAGE LOADER\n---\nUsage:\n\nimgload -c file.clf - Load CLF-format image file\nimgload -t file.tbl - Load TBL-format image file"

local args, opts = shell.parse(...)

local fp = ""
local isClf = false

local w, h = gpu.getResolution()
local wi, hi = 0, 0

if args[1] == nil then
  print(help)
  os.exit()
elseif opts["c"] then
  fp = args[1]
  isClf = true
elseif opts["t"] then
  fp = args[1]
  isClf = false
else
  print(help)
  os.exit()
end

gpu.setBackground(0x000000)
gpu.fill(1, 1, w, h, " ")
gpu.set(1, 1, "Loading")

img = ttf.load(fp)

if isClf then
  wi, hi = img[1], img[2]
else
  wi, hi = w, h
end

function runVideo ()
  local dti = 0
  if isClf then
    dti = 2
  end
  for yv=1,hi do
    for xv=1,wi do
      dti = dti + 1
      gpu.setBackground(img[dti])
      gpu.fill(xv, yv, 1, 1, " ")
      gpu.setBackground(0x000000)
    end
  end
end

runVideo()

while true do
  local id, _, x, y = event.pull("interrupted")
  gpu.setBackground(0x000000)
  os.execute("clear")
  os.exit()
end