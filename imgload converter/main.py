from PIL import Image
import numpy as np
import json

def image_to_pixel_array(image_path):
    img = Image.open(image_path).convert('RGB')
    pixel_array = np.array(img)
    pixels = [int(f'{r:02X}{g:02X}{b:02X}', 16) for r, g, b in pixel_array.reshape(-1, 3)]

    width, height = img.size
    pixels.insert(0, height)
    pixels.insert(0, width)

    return pixels

# Example usage
if __name__ == "__main__":
    image_path = input("Image path >> ")
    if image_path == "":
        image_path = "image.png"
    pixels = image_to_pixel_array(image_path)
    print(pixels[:2])
    if len(pixels) != pixels[0] * pixels[1] + 2:
        print("Error with image! Missing color data...")
    out = input("Export path (*.clf) >> ")
    if out == "":
        out = "image.clf"
    with open(out, "w") as f:
        f.write(json.dumps(pixels).replace("[","{").replace("]","}").replace(" ",""))
