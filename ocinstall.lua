local os = require("os")

print("ocinstall v1.0.1 - executing...")

-- libs
os.execute("wget https://gitlab.com/meltland/OCStuff/-/raw/main/tableToFile.lua /lib/tableToFile.lua")

-- programs
os.execute("wget https://gitlab.com/meltland/OCStuff/-/raw/main/dtf.lua /bin/dtf.lua")
os.execute("wget https://gitlab.com/meltland/OCStuff/-/raw/main/battery.lua /bin/battery.lua")
os.execute("wget https://gitlab.com/meltland/OCStuff/-/raw/main/imgload.lua /bin/imgload.lua")