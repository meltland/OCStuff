local ttf = require("tableToFile")
local component = require("component")
local c = component.computer
local shell = require("shell")
local os = require("os")

local args, opts = shell.parse(...)

local fp = ""

if args[1] == nil then
  print("Please provide a file.")
  os.exit()
else
  fp = args[1]
end


local bep = ttf.load(fp)

os.execute("clear")
print("Calculating file length...")

local totalsecs = 0

for k, v in pairs(bep["BEEPS"]) do
  totalsecs = totalsecs + v[2]
end

os.execute("clear")
print("NOW PLAYING\n---")
print(bep["META"]["title"])
print("by " .. bep["META"]["artist"])
print(tostring(totalsecs) .. " secs")

for k, v in pairs(bep["BEEPS"]) do
  local freq = v[1]
  local len = v[2]
  if freq == 0 then
    os.sleep(len)
  else
    c.beep(freq, len)
  end
end