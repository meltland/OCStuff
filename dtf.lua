local component = require("component")
local event = require("event")
local filesystem = require("filesystem")
local shell = require("shell")
local m = component.modem -- get primary modem component

local pv = "dtf1118" -- protocol version

local isSeek = false
local fp = "N/A"

local fcon = ""

local args = shell.parse(...)
if args[1] == nil then
  isSeek = true
else
  fp = args[1]
end

if not filesystem.exists("/hostname.dtf") then
  print("Hostname:")
  local hndt = io.read()
  if hndt == "" then
    os.exit()
  end
  local f = io.open("/hostname.dtf", "w")
  f:write(hndt)
  f:close()
end

if not isSeek then
  local dfcf = io.open(fp, "r")
  fcon = dfcf:read("*a")
  dfcf:close()
end

local hnf = io.open("/hostname.dtf", "r")
local hn = hnf:read("*l")
hnf:close()

print("DTF TRANSFER")
print("=======================")
print("PROTOCOL: " .. pv)
print("HOSTNAME: " .. hn)
print("FILE: " .. fp .. " (seeking: " .. tostring(isSeek) .. ")")
print("=======================")
print("Opening port...")
m.open(675)
print("Sharing hostname...")
m.broadcast(675, pv .. "::find:" .. hn)
print("Discovering...")

local hostname = ""
local addr = ""

local _, _, from, port, _, message = event.pull("modem_message")

if string.find(tostring(message), pv .. "::find:") and isSeek then
  hostname = string.gsub(tostring(message), pv .. "::find:", "")
  addr = from
  print("Discovered $" .. hostname .. ", sending match packet...")
  m.send(addr, 675, pv .. "::match:" .. hn)
elseif string.find(tostring(message), pv .. "::match:") and not isSeek then
  hostname = string.gsub(tostring(message), pv .. "::match:", "")
  addr = from
  print("Matched with $" .. hostname .. ", sending data packet...")
  m.send(addr, 675, pv .. "::content:" .. string.gsub(fcon, pv .. "::content:", ""))
  print("Sent!")
  os.exit()
else 
  if from == addr then
    print("Got unexpected packet from $" .. hostname .. "!")
  else
    print("Got unexpected packet from unmatched address " .. from .. "!")
  end
end

local _, _, from, port, _, message = event.pull("modem_message")

if string.find(tostring(message), pv .. "::content:") and from == addr and isSeek then
  print("Got data packet from $" .. hostname .. "!")
  local data = string.gsub(tostring(message), pv .. "::content:", "")
  print("File path (empty for " .. hostname .. ".txt):")
  local filename = io.read()
  if filename == "" then
    filename = hostname .. ".txt"
  end
  print("Saving as " .. filename .. "...")
  local frcv = io.open(filename, "w")
  frcv:write(data)
  frcv:close()
  print("Saved!")
else
  if from == addr then
    print("Got unexpected packet from $" .. hostname .. "!")
  else
    print("Got unexpected packet from unmatched address " .. from .. "!")
  end
end